﻿using UnityEngine;

public class LoseCollider : MonoBehaviour
{
	LevelManger LevelManger;

	void Start ()
	{
		LevelManger = Object.FindObjectOfType<LevelManger> ();
	}

	void OnTriggerEnter2D (Collider2D clldr)
	{
		LevelManger.LoadLevel ("Lose");
	}
}

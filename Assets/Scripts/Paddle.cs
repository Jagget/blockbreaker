﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour
{
	public bool AutoPlay = false;

	Ball Ball;

	// Use this for initialization
	void Start ()
	{
		Ball = Object.FindObjectOfType<Ball> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (AutoPlay) {
			DoAutoPlay ();
		} else {
			MoveWithMouse ();
		}
		
	}
	
	void MoveWithMouse ()
	{
		Vector3 PaddlePos = new Vector3 (0.0f, this.transform.position.y, this.transform.position.z);
		
		PaddlePos.x = Mathf.Clamp (Input.mousePosition.x / Screen.width * 16, 1.4f, 14.6f);
		
		this.transform.position = PaddlePos;
	}

	void DoAutoPlay ()
	{
		this.transform.position = new Vector3 (Ball.transform.position.x, this.transform.position.y, this.transform.position.z);
	}
}

﻿using UnityEngine;

public class Ball : MonoBehaviour
{
	Paddle Paddle;
	Vector3 PaddleToBallVector;
	bool HasNotStarted = true;
	Rigidbody2D theBody;
	Transform thisTransform;

	// Use this for initialization
	void Start ()
	{
		theBody = GetComponent<Rigidbody2D> ();
		thisTransform = GetComponent<Transform> ();
		Paddle = Object.FindObjectOfType<Paddle> ();
		PaddleToBallVector = thisTransform.position - Paddle.GetComponent<Transform> ().position;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (HasNotStarted) {
			thisTransform.position = Paddle.GetComponent<Transform> ().position + PaddleToBallVector;

			if (Input.GetMouseButtonDown (0)) {
				theBody.velocity = new Vector2 (2.5f, 9.5f);
				HasNotStarted = false;
			}
		}
	}
	
	Vector2 RotateVector3Deg (Vector2 vector, bool clockwise)
	{
		// 3  CW (0.999 x + 0.0523 y, 0.999 y - 0.0523 x)
		// 3 CCW (0.999 x - 0.0523 y, 0.0523 x + 0.999 y) 
		if (clockwise) {
			return new Vector2 (0.999f * vector.x + 0.0523f * vector.y, 0.999f * vector.y - 0.0523f * vector.x);
		}
		return new Vector2 (0.999f * vector.x - 0.0523f * vector.y, 0.0523f * vector.x + 0.999f * vector.y);
	}
	
	void OnCollisionEnter2D (Collision2D coll)
	{
		if (!HasNotStarted) {
			AudioSource.PlayClipAtPoint (GetComponent<AudioSource> ().clip, thisTransform.position);
			theBody.velocity = RotateVector3Deg (theBody.velocity, Random.Range (-0.3f, 0.3f) > 0);
		}
	}
}

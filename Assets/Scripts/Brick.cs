﻿using UnityEngine;

public class Brick : MonoBehaviour
{
	public Sprite[] HitSprites;
	public static int BricksCount = 0;
	public AudioClip Crack;
	public GameObject Smoke;
	bool IsBreakable;
	int MaxHits;
	int TimesHits;
	int SpriteIndex;
	LevelManger LevelMan;

	// Use this for initialization
	void Start ()
	{
		IsBreakable = tag == "Breakable";
		LevelMan = Object.FindObjectOfType<LevelManger> ();
		MaxHits = HitSprites.Length + 1;
		TimesHits = 0;
		
		if (IsBreakable) {
			BricksCount++;
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
	
	void OnCollisionEnter2D (Collision2D coll)
	{
		if (IsBreakable) {
			HandleHits ();
		}
	}
	
	void HandleHits ()
	{
		TimesHits++;
		if (TimesHits >= MaxHits) {
			BricksCount--;
			AudioSource.PlayClipAtPoint (Crack, transform.position);
			LevelMan.BrickDestroyed ();
			Puff ();
			Destroy (gameObject);
		} else {
			LoadSprites ();
		}	
	}
	
	void Puff ()
	{
		GameObject puff = Instantiate (Smoke, gameObject.transform.position, Quaternion.identity) as GameObject;
		puff.GetComponent<ParticleSystem> ().startColor = gameObject.GetComponent<SpriteRenderer> ().color;
		Destroy (puff, 1.5f);
	}

	void LoadSprites ()
	{
		SpriteIndex = TimesHits - 1;
		if (HitSprites [SpriteIndex]) {
			GetComponent<SpriteRenderer> ().sprite = HitSprites [SpriteIndex];
		} else {
			Debug.LogError ("No hit sprite!");
		}
	}
}

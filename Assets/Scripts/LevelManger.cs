﻿using UnityEngine;

public class LevelManger : MonoBehaviour
{
	public void LoadLevel (string levelName)
	{
		Debug.Log ("Level load requested for: " + levelName);
		Brick.BricksCount = 0;
		Application.LoadLevel (levelName);
	}

	public void LoadNextLevel ()
	{
		Application.LoadLevel (Application.loadedLevel + 1);
	}
	
	public void QuitRequest ()
	{
		Debug.Log ("Qiut requested");
		Application.Quit ();
	}
	
	public void BrickDestroyed ()
	{
		if (Brick.BricksCount <= 0) {
			LoadNextLevel ();
		}
	}
}
